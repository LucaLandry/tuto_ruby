class Book < ActiveRecord::Base
  belongs_to :category

  validates :title, presence: {
    message: "le titre du livre est obligatoire."
  }

  validates :title, uniqueness: {
    message: "Ce nom a déjà été entré"
  }
end
